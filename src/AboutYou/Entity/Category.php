<?php
declare(strict_types=1);

namespace AboutYou\Entity;


class Category
{
    /**
     * Id of the Category.
     *
     * @var int
     */
    protected $id;

    /**
     * Name of the Category.
     *
     * @var string
     */
    protected $name;

    /**
     * List of Products that belong to a Category.
     *
     * @var \AboutYou\Entity\Product[]
     */
    protected $products = [];

    /**
     * @param int $id
     *
     * @return Category
     */
    public function setId(int $id) : self
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @param string $name
     *
     * @return Category
     */
    public function setName(string $name) : self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @param array $products
     *
     * @return Category
     */
    public function setProducts(array $products) : self
    {
        foreach ($products as $id => $product) {
            $this->products[] = $this->createProduct($id, $product);
        }

        return $this;
    }

    /**
     * @return int
     */
    public function getId() : int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName() : string
    {
        return $this->name;
    }

    /**
     * @return array
     */
    public function getProducts() : array
    {
        return $this->products;
    }

    /**
     * @param int $id
     * @param array $product
     *
     * @return Product
     */
    public function createProduct(int $id, array $product) : Product
    {
        return (new Product)->setId($id)
                            ->setName($product['name'])
                            ->setDescription($product['description'])
                            ->createVariants($product['variants']);
    }
}
