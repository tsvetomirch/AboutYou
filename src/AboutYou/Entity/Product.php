<?php
declare(strict_types=1);

namespace AboutYou\Entity;

use AboutYou\Utilities\Validator;

class Product
{
    /**
     * Id of the Product.
     *
     * @var int
     */
    private $id;

    /**
     * Name of the Product.
     *
     * @var string
     */
    private $name;

    /**
     * Description of the Product.
     * 
     * @var string
     */
    private $description;

    /**
     * Unsorted list of Variants with their corresponding prices.
     * 
     * @var \AboutYou\Entity\Variant[]
     */
    private $variants = [];

    /**
     * @param int $id
     *
     * @return Product
     */
    public function setId(int $id) : self
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return int
     */
    public function getId() : int
    {
        return $this->id;
    }

    /**
     * @param string $name
     *
     * @return Product
     */
    public function setName(string $name) : self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return string
     */
    public function getName() : string
    {
        return $this->name;
    }

    /**
     * @param string $description
     *
     * @return Product
     */
    public function setDescription(string $description) : self
    {
        $this->description = $description;

        return $this;
    }

    /**
     * @return string
     */
    public function getDescription() : string
    {
        return $this->description;
    }

    /**
     * @param array $variants
     *
     * @return Product
     */
    public function createVariants(array $variants) : self
    {
        foreach ($variants as $id => $variant) {
            $this->variants[] = $this->createVariant($id, $variant);
        }

        return $this;
    }

    /**
     * Set already created variants for this product.
     *
     * @param array $variants
     * @return Product
     * @throws \Exception
     */
    public function setVariants(array $variants) : self
    {
        foreach ($variants as $variant) {
            if ($variant->getProduct()->getId() !== $this->getId()) {
                throw new \Exception('Setting wrong variants to product.');
            }
        }

        $this->variants = $variants;

        return $this;
    }

    /**
     * @return array
     */
    public function getVariants() : array
    {
        return $this->variants;
    }

    /**
     * @param int $id
     * @param array $variant
     *
     * @return Variant
     */
    private function createVariant(int $id, array $variant) : Variant
    {
        $rules = [
            'isDefault' => 'required|boolean',
            'isAvailable' => 'required|boolean',
            'size' => 'required',
            'price' => 'required|array',
            'quantity' => 'required|integer'
        ];

        Validator::validate($variant, $rules);

        return (new Variant())
            ->setId($id)
            ->setIsDefault($variant['isDefault'])
            ->setIsAvailable($variant['isAvailable'])
            ->setSize($variant['size'])
            ->setQuantity($variant['quantity'])
            ->setPrice($variant['price'])
            ->setProduct($this);
    }
}
