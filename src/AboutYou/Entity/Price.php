<?php

namespace AboutYou\Entity;


class Price
{
    /**
     * Current price.
     *
     * @var int
     */
    private $current;

    /**
     * Old price.
     *
     * @var int|null
     */
    private $old;

    /**
     * Defines if the price is sale.
     *
     * @var bool
     */
    private $isSale;

    /**
     * Variant that the price belongs to.
     *
     * @var \AboutYou\Entity\Variant
     */
    private $variant;

    /**
     * @return int
     */
    public function getCurrent(): int
    {
        return $this->current;
    }

    /**
     * @param int $current
     *
     * @return $this
     */
    public function setCurrent(int $current) : self
    {
        $this->current = $current;

        return $this;
    }

    /**
     * @return int|null
     */
    public function getOld() : int
    {
        return $this->old;
    }

    /**
     * @param int|null $old
     *
     * @return Price
     */
    public function setOld(?int $old) : self
    {
        $this->old = $old;

        return $this;
    }

    /**
     * @return bool
     */
    public function getIsSale(): bool
    {
        return $this->isSale;
    }

    /**
     * @param bool $isSale
     *
     * @return Price
     */
    public function setIsSale(bool $isSale) : self
    {
        $this->isSale = $isSale;

        return $this;
    }

    /**
     * @return Variant
     */
    public function getVariant(): Variant
    {
        return $this->variant;
    }

    /**
     * @param Variant $variant
     *
     * @return Price
     */
    public function setVariant(Variant $variant) : self
    {
        $this->variant = $variant;

        return $this;
    }
}
