<?php

namespace AboutYou\Services;

use AboutYou\Contracts\Services\ProductService as ProductServiceContract;
use AboutYou\Contracts\Services\CategoryService as CategoryServiceContract;

class UnorderedProductService implements ProductServiceContract
{
    /**
     * @var CategoryService
     */
    private $categoryService;

    /**
     * Maps from category name to the id for the category service.
     *  
     * @var array
     */
    private $categoryNameToIdMapping = [
        'Clothes' => 17325
    ];


    /**
     * UnorderedProductService constructor.
     *
     * @param CategoryServiceContract $categoryService
     */
    public function __construct(CategoryServiceContract $categoryService)
    {
       $this->categoryService = $categoryService;
    }

    /**
     * @param string $categoryName
     *
     * @return \AboutYou\Entity\Product[]
     */
    public function getProductsForCategory($categoryName)
    {
        if (!isset($this->categoryNameToIdMapping[$categoryName]))
        {
            throw new \InvalidArgumentException(sprintf('Given category name [%s] is not mapped.', $categoryName));
        }

        $categoryId = $this->categoryNameToIdMapping[$categoryName];

        return $productResults = $this->categoryService->getProducts($categoryId);
    }
}
