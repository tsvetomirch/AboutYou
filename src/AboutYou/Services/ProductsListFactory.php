<?php

namespace AboutYou\Services;

class ProductsListFactory
{
    /**
     * Simple factory for creating Products list. By default will be Unordered
     *
     * @param string $type
     * @return PriceOrderedProductService|UnorderedProductService
     */
    public static function make(string $type = null)
    {
        switch ($type) {
            case 'orderByPrice':
                return new PriceOrderedProductService(
                    new CategoryService()
                );
            break;

            default:
                return new UnorderedProductService(
                    new CategoryService()
                );
        }
    }
}