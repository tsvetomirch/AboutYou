<?php

namespace AboutYou\Services;

class PriceOrderedProductService extends UnorderedProductService
{
    /**
     * Using simple inheritance we are able to get the unordered list of products
     * and reorder it for our needs.
     *
     * @param string $categoryName
     *
     * @return \AboutYou\Entity\Product[]
     */
    public function getProductsForCategory($categoryName)
    {
        $products = parent::getProductsForCategory($categoryName);

        usort($products, function($firstProduct, $secondProduct) {
            $firstProductVariants = $firstProduct->getVariants();

            usort($firstProductVariants, function($firstVariant, $secondVariant) {
                return $firstVariant->getPrice()->getCurrent() <=> $secondVariant->getPrice()->getCurrent();
            });

            $firstProduct->setVariants($firstProductVariants);

            $secondProductVariants = $secondProduct->getVariants();
            usort($secondProductVariants, function($firstVariant, $secondVariant) {
                return $firstVariant->getPrice()->getCurrent() <=> $secondVariant->getPrice()->getCurrent();
            });

            $secondProduct->setVariants($secondProductVariants);

            return $firstProductVariants[0]->getPrice()->getCurrent() <=> $secondProductVariants[0]->getPrice()->getCurrent();
        });

        return $products;
    }
}