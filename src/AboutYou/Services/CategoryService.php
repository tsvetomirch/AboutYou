<?php
declare(strict_types=1);

namespace AboutYou\Services;

use AboutYou\Contracts\Services\CategoryService as CategoryContract;
use AboutYou\Entity\Category;

class CategoryService implements CategoryContract
{

    /**
     * This method should read from a data source (JSON in our case)
     * and return an unsorted list of products found in the data source.
     *
     * @param integer $categoryId
     *
     * @return \AboutYou\Entity\Product[]
     */
    public function getProducts(int $categoryId)
    {
        if ($categoryId < 1) {
            throw new \InvalidArgumentException("Category ID should be greater than 0.");
        }

        /**
         * This can be done with file wrapper passed through the constructor or method
         * but it is out of the scope for this task.
         */
        $fileContent = file_get_contents('data/17325.json');
        $data = json_decode($fileContent, true);
        if ($data['id'] === $categoryId) {
            return (new Category())
                ->setId($data['id'])
                ->setName($data['name'])
                ->setProducts($data['products'])
                ->getProducts();
        }

        return [];
    }
}