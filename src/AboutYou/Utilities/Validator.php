<?php
declare(strict_types=1);

namespace AboutYou\Utilities;

use AboutYou\Contracts\Validators\Validator as ValidatorContract;
use AboutYou\Exceptions\ValidationException;

class Validator implements ValidatorContract
{
    /**
     * @var
     */
    protected static $data;

    /**
     * @var array
     */
    private static $rulesMapping = [
        'required' => 'requiredRule',
        'boolean'     => 'is_bool',
        'integer'  => 'is_integer',
        'array'    => 'is_array',
        'string'   => 'is_string'
    ];

    /**
     * @var array
     */
    private static $rulesMessages = [
        'requiredRule' => 'The field %s is required.',
        'is_bool'      => 'The field %s must be a boolean.',
        'is_integer'   => 'The field %s must be an integer.',
        'is_array'     => 'The field %s must be an array.',
        'is_string'    => 'The field %s must be a string.',
    ];

    /**
     * @param array $data
     * @param array $validationRules
     *
     * @return bool
     * @throws ValidationException
     */
    static function validate(array $data, array $validationRules) : bool
    {
        self::$data = $data;

        foreach ($validationRules as $field => $rules) {
            $functionsToApply = self::parseRules($rules);

            foreach ($functionsToApply as $func) {
                if (method_exists(get_called_class(), $func)) {
                    $result = self::{"$func"}($field);
                    if ($result) {
                        continue;
                    }
                }

                if (!$func($data[$field])) {
                    throw new ValidationException(
                        sprintf(self::$rulesMessages[$func], $field)
                    );
                }
            }
        }

        return true;
    }

    /**
     * @param string $rules
     *
     * @return array
     * @throws ValidationException
     */
    private static function parseRules(string $rules) : array
    {
        $rules = explode('|', $rules);
        if (count($rules) == 1 && $rules[0] === '') {
            throw new ValidationException('No validation rule to apply.');
        }

        $functions = [];

        foreach ($rules as $rule) {
            if (!key_exists($rule, self::$rulesMapping)) {
               throw new ValidationException("Rule {$rule} is not supported!");
            }
            $functions[] = self::$rulesMapping[$rule];
        }

        return $functions;
    }

    /**
     * As key_exists requires two params we create a custom function to unify it.
     *
     * @param string $field
     *
     * @return bool
     * @throws ValidationException
     */
    private static function requiredRule(string $field) : bool
    {
        if (!key_exists($field, self::$data)) {
            throw new ValidationException(
                sprintf(self::$rulesMessages['requiredRule'], $field)
            );
        }

        return true;
    }
}