<?php

namespace AboutYou\Contracts\Services;


interface CategoryService
{
    /**
     * This method should read from a data source (JSON in our case)
     * and return an unsorted list of products found in the data source.
     * 
     * @param integer $categoryId
     *
     * @return \AboutYou\Entity\Product[]
     */
    public function getProducts(int $categoryId);
}
