<?php

namespace AboutYou\Contracts\Validators;

interface Validator
{
    /**
     * @param array $data
     * @param array $rules
     * @return bool
     */
    static function validate(array $data, array $rules) : bool;
}