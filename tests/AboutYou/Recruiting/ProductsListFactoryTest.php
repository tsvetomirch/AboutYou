<?php

namespace Tests\Recruiting;

use AboutYou\Entity\Product;
use AboutYou\Services\PriceOrderedProductService;
use AboutYou\Services\ProductsListFactory;
use AboutYou\Services\UnorderedProductService;
use PHPUnit\Framework\TestCase;

class ProductsListFactoryTest extends TestCase
{
    public function test_factory_create_unordered_list_by_default()
    {
        $list = ProductsListFactory::make();
        $this->assertTrue($list instanceof UnorderedProductService);
        $productsList = $list->getProductsForCategory("Clothes");
        $this->assertNotEmpty($productsList);
        $this->assertTrue($productsList[0] instanceof Product);
        $newList = ProductsListFactory::make('something');
        $this->assertTrue($newList instanceof UnorderedProductService);
    }

    public function test_factory_is_able_to_create_ordered_list()
    {
        $list = ProductsListFactory::make('orderByPrice');
        $this->assertTrue($list instanceof PriceOrderedProductService);
    }
}