<?php

namespace Tests\Recruiting;

use AboutYou\Entity\Product;
use AboutYou\Services\CategoryService;
use AboutYou\Services\UnorderedProductService;
use PHPUnit\Framework\TestCase;

class UnorderedProductServiceTest extends TestCase
{
    protected $category;
    protected $list;

    public function setUp()
    {
        $this->category = new CategoryService();
        $this->list = new UnorderedProductService($this->category);
    }

    public function test_returns_list_of_products()
    {
        $listOfClothes = $this->list->getProductsForCategory('Clothes');
        $this->assertNotEmpty($listOfClothes);
        $this->assertSame(get_class($listOfClothes[0]), Product::class);
    }

    public function test_exception_will_be_thrown_if_category_name_is_not_mapped()
    {
        $exceptionRaised = false;

        try {
            $listOfUnknown = $this->list->getProductsForCategory('Unknown');
        } catch (\Exception $e) {
            $exceptionRaised = true;

            $this->assertSame(\InvalidArgumentException::class, get_class($e));
            $this->assertSame('Given category name [Unknown] is not mapped.', $e->getMessage());
        }

        $this->assertTrue($exceptionRaised);

    }
}