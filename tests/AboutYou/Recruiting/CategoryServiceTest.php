<?php

namespace Tests\Recruiting;

use AboutYou\Entity\Price;
use AboutYou\Entity\Product;
use AboutYou\Entity\Variant;
use AboutYou\Services\CategoryService;
use PHPUnit\Framework\TestCase;

class CategoryServiceTest extends TestCase
{
    protected $categoryService;

    public function setUp()
    {
        $this->categoryService = new CategoryService();
    }

    public function test_proper_category_id()
    {
        $results = $this->categoryService->getProducts(17325);

        foreach ($results as $product) {
            $this->assertSame(get_class($product), Product::class);
            foreach ($product->getVariants() as $variant) {
                $this->assertSame(get_class($variant), Variant::class);
                $this->assertSame($product, $variant->getProduct());
                $this->assertSame(get_class($variant->getPrice()), Price::class);
                $this->assertSame($variant->getPrice()->getVariant(), $variant);
            }
        }

        $this->assertEquals(2, count($results));
    }

    public function test_nothing_is_returned_if_no_category_available()
    {
        $result = $this->categoryService->getProducts(1);

        $this->assertEquals([], $result);
    }

    public function test_category_id_should_be_greater_than_zero()
    {
        $exceptionRaised = false;

        try {
            $this->categoryService->getProducts(0);
        } catch (\Exception $e) {
            $exceptionRaised = true;

            $this->assertSame(\InvalidArgumentException::class, get_class($e));
            $this->assertSame('Category ID should be greater than 0.', $e->getMessage());
        }

        $this->assertTrue($exceptionRaised);

        try {
            $this->categoryService->getProducts(-1);
        } catch (\Exception $e) {
            $this->assertSame(\InvalidArgumentException::class, get_class($e));
            $this->assertSame('Category ID should be greater than 0.', $e->getMessage());
        }
    }
}