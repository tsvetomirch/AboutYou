<?php

namespace Tests\Recruiting;

use AboutYou\Entity\Product;
use AboutYou\Services\CategoryService;
use AboutYou\Services\PriceOrderedProductService;
use PHPUnit\Framework\TestCase;

class PriceOrderedProductServiceTest extends TestCase
{
    protected $category;
    protected $list;

    public function setUp()
    {
        $this->category = new CategoryService();
        $this->list = new PriceOrderedProductService($this->category);
    }

    public function test_returns_list_of_price_ordered_products()
    {
        $productsList = $this->list->getProductsForCategory('Clothes');
        $this->assertNotEmpty($productsList);
        $this->assertSame(get_class($productsList[0]), Product::class);

        $firstProductVariantPrice = $productsList[0]->getVariants()[0]->getPrice()->getCurrent();
        $firstProductSecondVariantPrice = $productsList[0]->getVariants()[1]->getPrice()->getCurrent();

        $this->assertTrue($firstProductVariantPrice < $firstProductSecondVariantPrice);
    }

    public function test_exception_will_be_thrown_if_category_name_is_not_mapped()
    {
        $exceptionRaised = false;

        try {
            $productsList = $this->list->getProductsForCategory('Unknown');
        } catch (\Exception $e) {
            $exceptionRaised = true;

            $this->assertSame(\InvalidArgumentException::class, get_class($e));
            $this->assertSame('Given category name [Unknown] is not mapped.', $e->getMessage());
        }

        $this->assertTrue($exceptionRaised);

    }
}