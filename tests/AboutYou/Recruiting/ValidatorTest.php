<?php

namespace Tests\Recruiting;

use AboutYou\Exceptions\ValidationException;
use AboutYou\Utilities\Validator;
use PHPUnit\Framework\TestCase;

class ValidatorTest extends TestCase
{
    protected $rules;
    protected $data;

    public function setUp()
    {
        $this->rules = [
            'TestField1' => 'required|boolean',
            'TestField2' => 'required|array',
        ];

        $this->data = [
            'TestField1' => true,
            'TestField2' => []
        ];
    }

    public function test_it_throws_exception_if_rule_is_not_suppoerted()
    {
        $this->rules['TestField3'] = 'required|fail';

        try {
            Validator::validate($this->data, $this->rules);
        } catch (\Exception $e) {
            $this->assertSame(ValidationException::class, get_class($e));
            $this->assertSame('Rule fail is not supported!', $e->getMessage());
        }
    }

    public function test_all_passes()
    {
        $result = Validator::validate($this->data, $this->rules);
        $this->assertTrue($result);
    }

    public function test_boolean_rule()
    {
        $this->rules['TestField3'] = 'required|boolean';
        $this->data['TestField3'] = 'This is string.';

        try {
            Validator::validate($this->data, $this->rules);
        } catch (\Exception $e) {
            $this->assertSame(ValidationException::class, get_class($e));
            $this->assertSame('The field TestField3 must be a boolean.', $e->getMessage());
        }
    }

    public function test_string_rule()
    {
        $this->rules['TestField3'] = 'required|string';
        $this->data['TestField3'] = 1;

        try {
            Validator::validate($this->data, $this->rules);
        } catch (\Exception $e) {
            $this->assertSame(ValidationException::class, get_class($e));
            $this->assertSame('The field TestField3 must be a string.', $e->getMessage());
        }
    }

    public function test_required_rule()
    {
        $this->rules['TestField3'] = 'required';

        try {
            Validator::validate($this->data, $this->rules);
        } catch (\Exception $e) {
            $this->assertSame(ValidationException::class, get_class($e));
            $this->assertSame('The field TestField3 is required.', $e->getMessage());
        }
    }

    public function test_integer_rule()
    {
        $this->rules['TestField3'] = 'required|integer';
        $this->data['TestField3'] = 'string';

        try {
            Validator::validate($this->data, $this->rules);
        } catch (\Exception $e) {
            $this->assertSame(ValidationException::class, get_class($e));
            $this->assertSame('The field TestField3 must be an integer.', $e->getMessage());
        }
    }

    public function test_array_rule()
    {
        $this->rules['TestField3'] = 'required|array';
        $this->data['TestField3'] = 'string';

        try {
            Validator::validate($this->data, $this->rules);
        } catch (\Exception $e) {
            $this->assertSame(ValidationException::class, get_class($e));
            $this->assertSame('The field TestField3 must be an array.', $e->getMessage());
        }
    }
}